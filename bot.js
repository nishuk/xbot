const Discord = require('discord.js');
const config = require('./config');
const riot = require('./src/riot');
const audio = require('./src/audio');

// champions will be loaded to this variable
var champions = [];
riot.getChampions().then(data => {
  champions = data;
  console.log('Champions fetched! Except if you saw errors.')
});
/*
lol players structure goes like this:
[
  {
    discordId: 242122221212
    accountId: 25995379,
    lastMatchId: LAST_MATCH
  },
  ...
]
*/
var lolPlayers = [];
const voiceConnections = [];

const client = new Discord.Client();

client.on('ready', () => {
  console.log('xBot ready!');
});

client.on('message', message => {
  callMessageCommand(message);
})

client.login(config.TOKEN);

// TODO: Summoner not found / errors during fetch
const addLolAccountCmd = (message, parameters) => {
  if (parameters.length === 0) {
    message.channel.send('addLolAccount takes your summoner name as parameter. For example "!addLolAccount -rahuksi".');
    return;
  }
  let summonerName = parameters[0];
  riot.getSummonerByName(summonerName)
    .then(data => {
      let accountId = data.accountId;
      if (lolPlayers.find(player => player.accountId === accountId) !== undefined) {
        message.channel.send(`${summonerName} is already added.`);
        return;
      }
      riot.getLastMatch(accountId)
        .then(data => {
          lolPlayers.push({
            discordId: message.author.id,
            accountId: accountId,
            lastMatchId: data.gameId
          });
          message.channel.send(`${summonerName} added.`);
        });
    });    
}

const joinChannelCmd = message => {
  if (!message.member.voiceChannel) {
    message.reply('join a channel first!')
    return;
  }
  let voiceChannel = message.member.voiceChannel;
  voiceChannel.join()
    .then(connection => {
      voiceConnections.push(connection);
      message.reply(`joined ${connection.channel.name}`);
    }).catch(error => {
      message.reply('error joining channel!')
    })
}

const disconnectCmd = message => {
  if (!message.member.voiceChannel) {
    message.reply('join a channel first or use !disconnectAll to disconnect from all channels.');
    return;
  }
  let connection = voiceConnections.find(conn => conn.channel === message.member.voiceChannel);
  if (!connection) {
    message.reply('no xBot instance found on your channel. Use !disconnectAll to disconnect from all channels.');
    return;
  }
  let channelName = connection.channel.name;
  connection.disconnect();
  let index = voiceConnections.indexOf(connection);
  voiceConnections.splice(index, 1);
  message.reply(`disconnected from ${channelName}.`)
}

const disconnectAllCmd = message => {
  let count = voiceConnections.length;
  if (count === 0) {
    message.reply('xBot is not active on any voice channel!');
    return;
  }
  for (let i = 0; i < voiceConnections.length; i++) {
    voiceConnections[i].disconnect();
  }
  voiceConnections.length = 0;
  message.reply(`disconnected from ${count} channels!`)
}

const playCmd = (message, parameters) => {
  if (parameters.length === 0) {
    message.reply('you must supply sound name to play as a parameter. For example "!xPlay -mulleRiitti".');
    return;
  }
  let connection = voiceConnections.find(conn => conn.channel === message.member.voiceChannel);
  if (!connection) {
    message.reply('xBot needs to be on the same voice channel as you to play sounds. Use "!join" to invite xBot to your current channel.');
    return;
  }
  let soundName = parameters[0];
  let audioClip = audio[soundName];
  if (audioClip === undefined) {
    message.reply(`${soundName} not found.`);
    return;
  }
  const dispatcher = connection.playFile(`./src/audio/${audioClip.src}.mp3`);
}

const listSoundsCmd = message => {
  let whiteSpaceCount = 20;
  let blockString = '```';
  let keys = Object.keys(audio);
  let string = blockString;
  string = string + 'Komento             Kuvaus';
  string = string + '\n--------------------------------------------------------'
  for (let i = 0; i < keys.length; i++) {
    let totalWhitespaceCount = whiteSpaceCount - keys[i].length;
    string = string + '\n' + keys[i];
    for (let j = 0; j < totalWhitespaceCount; j++) {
      string = string + ' ';
    }
    string = string + audio[keys[i]].description;
  }
  string = string + blockString;
  message.reply(string);
}

const helpCmd = message => {
  let whiteSpaceCount = 20;
  let blockString = '\n```';
  let string = blockString;
  string = string + 'Komento             Parametrit          Kuvaus';
  string = string + '\n------------------------------------------------------------------------------------------';
  string = string + '\n!xjoin              Ei ole              xBot liittyy samalle channelille kanssasi.';
  string = string + '\n!xdisconnect        Ei ole              xBot poistuu channeliltasi.';
  string = string + '\n!xdisconnectAll     Ei ole              xBot poistuu kaikilta channeleilta.';
  string = string + '\n!xplay              Klipin nimi         Soittaa klipin. Listan saat alemmalla komennolla.';
  string = string + '\n!xlist              Ei ole              Listaa klipit.';
  string = string + '\n!xhelp              Ei ole              Näyttää tämän inforuudun.';
  string = string + '\n------------------------------------------------------------------------------------------';
  string = string + '\nParametrit erotellaan viivalla, esim "!xplay -eiMua".';
  string = string + '\nJos haluat soittaa klippejä, xBotin on oltava samalla channelilla kanssasi.';
  string = string + blockString;
  message.reply(string);
}

const callMessageCommand = message => {
  let messageContentString = message.content;
  messageContentString = messageContentString.trimLeft();
  if (message.channel.type !== 'text' || message.channel.name !== 'xbot' || messageContentString.length === 0 || messageContentString.charAt(0) !== '!') {
    return;
  }
  let whiteSpaceIndex = messageContentString.indexOf(' ');
  let commandString = messageContentString;
  let parameters = [];
  if (whiteSpaceIndex > 0) {
    commandString = commandString.substring(0, whiteSpaceIndex);
    let parameterString = messageContentString.substring(whiteSpaceIndex, messageContentString.length);
    if (parameterString.length > 1) {
      parameterString = parameterString.substring(1, parameterString.length);
    }
    parameters = parameterString.split('-')
    for (let i = 0; i < parameters.length; i++) {
      parameters[i] = parameters[i].trim();
    }
    parameters = parameters.filter(parameter => parameter.length > 0);
  }
  
  switch (commandString.toLowerCase()) {
    case '!xjoin':
      joinChannelCmd(message);
      break;
    case '!xdisconnect':
      disconnectCmd(message);
      break;
    case '!xdisconnectall':
      disconnectAllCmd(message);
      break;
    case '!xplay':
      playCmd(message, parameters);
      break;
    case '!xlist':
      listSoundsCmd(message);
      break;
    case '!xhelp':
      helpCmd(message);
      break;
  }
}
