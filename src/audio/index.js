module.exports = {
  'bitches': {
    src: '6_tappoa_bitches',
    description: 'Kateellista sakkia.'
  },
  'ammu': {
    src: 'ammu',
    description: 'Kun kaveri ei ammu.'
  },
  'apina': {
    src: 'apina',
    description: 'Ei ollu oma syy.'
  },
  'eiMua': {
    src: 'ei_mua',
    description: 'Vesku menee taas.'
  },
  'jee': {
    src: 'jee',
    description: 'Enemy has been slain.'
  },
  'juhoRage': {
    src: 'juho_voi_vittu',
    description: 'Edes rage ei onnistu.'
  },
  'valot': {
    src: 'juhon_valot',
    description: 'Valot oli ihan hyvät.'
  },
  'kulliAija': {
    src: 'kulli_aija',
    description: 'No context.'
  },
  'missaVesku': {
    src: 'missa_vesku',
    description: 'Veskut löytyy yllättävistä paikoista.'
  },
  'mulleRiitti': {
    src: 'mulle_riitti',
    description: 'Alkoi väsyttämään.'
  },
  'aksunPalaute': {
    src: 'palaute_20db',
    description: 'Aksu antaa suoran palautteen.'
  },
  'pudge1': {
    src: 'pudge1',
    description: 'Pudge alkaa pelottamaan.'
  },
  'pudge2': {
    src: 'pudge2',
    description: 'Pudge löytyy puskasta.'
  },
  'tibbers': {
    src: 'tibbers',
    description: 'Aksu yrittää tehä playta.'
  },
  'komeasti': {
    src: 'todella_komeasti',
    description: 'Kun kombo onnistuu.'
  },
  'vesku1': {
    src: 'vesku1',
    description: 'Veskun rage v1.'
  },
  'vesku2': {
    src: 'Veskun_Valitus2',
    description: 'Veskun rage v2'
  },
  'sori1': {
    src: 'sori_markus',
    description: 'Vesku menee hetkessä.'
  },
  'sori2': {
    src: 'SoriMarkus2',
    description: 'Vesku vauhdissa taas'
  },
  'sonkotys': {
    src: 'VeskuSönkötys',
    description: 'Oot sää humalassa?'
  },
  'goGoAttack': {
    src: 'GoGoAttack',
    description: 'Veskua kyllästyttää.'
  },
  'juhoAnsaan': {
    src: 'JuhoAnsaan',
    description: 'Karma strikes back.'
  },
  'klonkku1': {
    src: 'Klonkku1',
    description: 'Ei kai'
  },
  'klonkku2': {
    src: 'Klonkku2',
    description: 'Ei kai taas'
  },
};
