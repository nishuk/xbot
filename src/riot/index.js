const axios = require('axios');
const config = require('./config');

const SUMMONER_URL = 'https://euw1.api.riotgames.com/lol/summoner/v3';
const MATCH_URL = 'https://euw1.api.riotgames.com/lol/match/v3';
const DDRAGON_CHAMPIONS_URL = 'http://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US/champion.json';
const API_KEY_PARAMETER = `api_key=${config.API_KEY}`;

const getErrorMessage = statusCode => {
  switch (statusCode) {
    case 400:
      return 'Bad request'
    case 401:
      return 'Unauthorized'
    case 403:
      return 'Forbidden'
    case 404:
      return 'Data no found'
    case 405:
      return 'Method not allowed'
    case 415:
      return 'Unsupported media type'
    case 422:
      return 'Player exists, but has not played since match history collection began'
    case 429:
      return 'Rate limit exceeded'
    case 500:
      return 'Internal server error'
    case 502:
      return 'Bad gateway'
    case 503:
      return 'Service unavailable'
    case 504:
      return 'Gateway timeout'
    default:
      return 'Unknown error'
  }
}

const logError = (error, funcName) => {
  let status = error.response.status;
  console.log(`${funcName} request failed with status ${status}/${getErrorMessage(status)}`);
}

module.exports = {
  getSummonerByName: name => {
    return axios.get(`${SUMMONER_URL}/summoners/by-name/${name}?${API_KEY_PARAMETER}`)
      .then(res => {
        return res.data;
      }).catch(error => {
        logError(error, 'getSummonerByName');
      });
  },
  getMatchList: (accountId, count) => {
    return axios.get(`${MATCH_URL}/matchlists/by-account/${accountId}?endIndex=${count}&${API_KEY_PARAMETER}`)
      .then(res => {
        return res.data.matches;
      }).catch(error => {
        logError(error, 'getMatchList');
      });
  },
  getLastMatch: accountId => {
    return axios.get(`${MATCH_URL}/matchlists/by-account/${accountId}?endIndex=${1}&${API_KEY_PARAMETER}`)
      .then(res => {
        return res.data.matches.length > 0 ? res.data.matches[0] : undefined;
      }).catch(error => {
        logError(error, 'getLastMatch');
      });
  },
  getMatch: matchId => {
    return axios.get(`${MATCH_URL}/matches/${matchId}?${API_KEY_PARAMETER}`)
      .then(res => {
        return res.data;
      }).catch(error => {
        logError(error, 'getMatch');
      });
  },
  getChampions: () => {
    return axios.get(DDRAGON_CHAMPIONS_URL)
      .then(res => {
        let data = res.data.data;
        return Object.keys(data).map(champ => data[champ]);
      }).catch(error => {
        console.log('Error tring to fetch champions from data dragon.');
      });
  }
};
